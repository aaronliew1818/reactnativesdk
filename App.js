import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import ToastExample from './ToastExample';

export default function App() {
  ToastExample.show('Awesome', ToastExample.SHORT);
  return (
    <View style={styles.container}>
      <Text>Start working on this!</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
